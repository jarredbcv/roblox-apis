<?php
	/*
	Use in conjunction with send.lua.
	For more instructions on usage read the instructions in the parent folder's README.
	
	To use this move it out of the entire folder.
	(If you want to change it to be used from inside this folder change base to '..')
	Put your own get key and post key and put those in your ROBLOX sending file.
	*/
	$getKey = 'asd321';
	$postKey = 'asd123';
	if (!$_GET || !array_key_exists('key',$_GET) || $_GET['key'] != $getKey) {
		die('FAILURE: Incorrect/missing validation key.');
	}
	if (!$_GET || !array_key_exists('group',$_GET)){
		die('FAILURE: Incorrect/missing group id.');
	}
	$base = '../';
	include_once $base.'/Includes/getRoles.php';
	include_once $base.'/Includes/login.php';
	include_once $base.'/Includes/getPostData.php';
	include_once $base.'/changeRank.php';
	include_once $base.'/shout.php';
	include_once $base.'/exile.php';
	include_once $base.'/message.php';
	
	libxml_use_internal_errors(true); 
	$group = $_GET['group'];
	$cookieTime = $base.'/Private/cookieTime.txt';
	if (!file_exists($cookieTime)) {
		file_put_contents($cookieTime,0);
	}
	$cookie = $base.'/Private/cookie';
	if (time()-file_get_contents($cookieTime) > 86400) {
		login($cookie,'TMC_Administrator','Th3M0rt3mC0lt');
		file_put_contents($cookieTime,time());
	}
	$data = getPostData(true);
	if (!$data || !array_key_exists('Validate',$data) || $data['Validate'] != $postKey) {
		die('FAILURE: Incorrect/missing validation key.');
	}
	switch($data['Action']) {
		case 'setRank':
			list($ranks,$roles) = getRoleSets($group);
			echo updateRank($group,$data['Parameter1'],$data['Parameter2'],$cookie,$ranks,$roles,9,$base.'/Private/gxcsrf.txt');
			break;
		case 'exile':
			 echo exile($cookie,$group,$data['Parameter1'],$data['Parameter2'],$deletePosts = false,$save='../Private/excsrf.txt');
			 break;
		case 'message':
			message($cookie,$data['Parameter1'],$data['Parameter2'],$data['Parameter3'],$save='../Private/mxcsrf.txt');
			break;
		case 'shout':
			echo shout($cookie,$group,$data['Parameter1']);
			break;
		default:
			die('No action!');
	}
?>
